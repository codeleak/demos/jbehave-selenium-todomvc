Creating Todo

Scenario: Create Todo

Given user is on TodoMVC page
When user enters <todo>
Then todo with name <todo> is on the todo list

Examples:
|todo|
|Buy the Milk|
|Write letter to Santa|
|Finish homework|