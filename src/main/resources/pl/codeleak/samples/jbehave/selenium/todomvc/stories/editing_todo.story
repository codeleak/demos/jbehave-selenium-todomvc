Editing Todo

Scenario: Rename Todo

Given user is on TodoMVC page
And there are todos on the todo list:
|todo|
|Buy the Milk|
|Write letter to Santa|
|Finish homework|

When user renames <todo> to <new_todo>
Then todo with name <new_todo> is on the todo list
And todo with name <todo> is not on the todo list

Examples:
|todo|new_todo|
|Buy the Milk|Buy the Book|
|Write letter to Santa|Write letter to a Friend|
|Finish homework|Finish the game|