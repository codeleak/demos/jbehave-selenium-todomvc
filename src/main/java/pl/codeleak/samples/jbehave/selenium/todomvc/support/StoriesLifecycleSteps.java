package pl.codeleak.samples.jbehave.selenium.todomvc.support;

import org.jbehave.core.annotations.*;
import org.jbehave.web.selenium.WebDriverProvider;
import org.openqa.selenium.JavascriptExecutor;

public class StoriesLifecycleSteps {

    private WebDriverProvider webDriverProvider;

    public StoriesLifecycleSteps(WebDriverProvider webDriverProvider) {
        this.webDriverProvider = webDriverProvider;
    }

    @AfterScenario(uponType = ScenarioType.ANY)
    public void beforeStory() {
        webDriverProvider.get().manage().deleteAllCookies();
        ((JavascriptExecutor) webDriverProvider.get()).executeScript("localStorage.clear()");
    }
}
