package pl.codeleak.samples.jbehave.selenium.todomvc.steps;

import org.jbehave.core.annotations.*;
import org.jbehave.core.model.ExamplesTable;
import pl.codeleak.samples.jbehave.selenium.todomvc.pages.TodoMvc;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class TodoMvcSteps {

    private TodoMvc todoMvc;

    public TodoMvcSteps(TodoMvc todoMvc) {
        this.todoMvc = todoMvc;
    }

    @Given("user is on TodoMVC page")
    public void givenUserIsOnTodoMVCPage() {
        todoMvc.navigateTo();
    }

    @Given("there are todos on the todo list: $todos")
    public void whenThereAreTodosOnTheTodoList(ExamplesTable todos) {
        todos.getRows().forEach(
                row -> {
                    todoMvc.createTodo(row.get("todo"));
                }
        );
        assertThat(todoMvc.getTodoCount(), equalTo(todos.getRowCount()));
        assertThat(todoMvc.getTodosLeft(), equalTo(todos.getRowCount()));
    }

    @When("user enters $todo")
    public void whenUserEntersTodo(@Named("todo") String todo) {
        todoMvc.createTodo(todo);
    }

    @When("user renames $todo to $new_todo")
    public void whenUserRenamesTodo(@Named("todo") String todo, @Named("new_todo") String newTodo) {
        todoMvc.renameTodo(todo, newTodo);
    }

    @Then("todo with name $todo is on the todo list")
    public void thenTodoIsOnTheList(@Named("todo") String todo) {
        assertThat(todoMvc.getTodos(), hasItem(todo));
    }

    @Then("todo with name $todo is not on the todo list")
    public void thenTodoIsNotOnTheList(@Named("todo") String todo) {
        assertThat(todoMvc.getTodos(), not(hasItem(todo)));
    }

    @When("user completes $todo")
    public void userCompletesTodo(@Named("todo") String todo) {
        todoMvc.completeTodo(todo);
    }

    @Then("todo with name $todo is on the completed todo list")
    public void todoIsOnTheCompletedTodoList(@Named("todo") String todo) {
        todoMvc.showCompleted();
        assertThat(todoMvc.getTodos(), hasItem(todo));
    }

    @Then("todo with name $todo is not on the active todo list")
    public void todoIsNotOnTheActiveTodoList(@Named("todo") String todo) {
        todoMvc.showActive();
        assertThat(todoMvc.getTodos(), not(hasItem(todo)));
    }

    @When("user clears completed todos")
    public void userClearsCompletedTodos() {
        todoMvc.clearCompleted();
    }

    @Then("there are $count left items on the todo list")
    @Alias("there is $count left item on the todo list")
    public void thereIsNumberOfLeftItemsOnTheTodoList(@Named("count") int count) {
        assertThat(todoMvc.getTodosLeft(), equalTo(count));
    }

}
