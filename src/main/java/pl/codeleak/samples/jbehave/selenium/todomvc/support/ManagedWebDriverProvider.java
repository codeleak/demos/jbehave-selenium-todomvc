package pl.codeleak.samples.jbehave.selenium.todomvc.support;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.jbehave.web.selenium.DelegatingWebDriverProvider;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class ManagedWebDriverProvider extends DelegatingWebDriverProvider {

    private static final Map<String, Class<? extends RemoteWebDriver>> drivers = Map.of(
            "chrome", ChromeDriver.class,
            "firefox", FirefoxDriver.class,
            "safari", SafariDriver.class);

    private static final int DEFAULT_TIMEOUT_IN_MILLIS = 500;

    @Override
    public void initialize() {
        delegate.set(newRemoteWebDriver(System.getProperty("browser", "firefox")));
    }

    private RemoteWebDriver newRemoteWebDriver(String browser) {
        Class<? extends RemoteWebDriver> driverClass = drivers.get(browser);

        if (driverClass == null) {
            throw new IllegalArgumentException("Not supported browser " + browser);
        }

        try {
            WebDriverManager.getInstance(driverClass).setup();
            RemoteWebDriver remoteWebDriver = driverClass.getDeclaredConstructor().newInstance();
            remoteWebDriver.manage().timeouts().implicitlyWait(DEFAULT_TIMEOUT_IN_MILLIS, TimeUnit.MILLISECONDS);
            return remoteWebDriver;
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            throw new RuntimeException("Could not initialize driver!");
        }
    }

}
