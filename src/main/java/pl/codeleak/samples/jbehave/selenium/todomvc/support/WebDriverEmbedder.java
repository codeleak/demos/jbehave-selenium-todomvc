package pl.codeleak.samples.jbehave.selenium.todomvc.support;

import org.jbehave.core.configuration.Configuration;
import org.jbehave.core.embedder.Embedder;
import org.jbehave.core.embedder.EmbedderControls;
import org.jbehave.core.embedder.StoryControls;
import org.jbehave.core.embedder.executors.DirectExecutorService;
import org.jbehave.core.io.LoadFromClasspath;
import org.jbehave.core.reporters.CrossReference;
import org.jbehave.core.reporters.FilePrintStreamFactory;
import org.jbehave.core.reporters.StoryReporterBuilder;
import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;
import org.jbehave.core.steps.ParameterConverters;
import org.jbehave.web.selenium.PerStoriesWebDriverSteps;
import org.jbehave.web.selenium.SeleniumConfiguration;
import org.jbehave.web.selenium.WebDriverProvider;
import org.jbehave.web.selenium.WebDriverSteps;
import pl.codeleak.samples.jbehave.selenium.todomvc.pages.TodoMvc;
import pl.codeleak.samples.jbehave.selenium.todomvc.steps.TodoMvcSteps;

import java.text.SimpleDateFormat;
import java.util.concurrent.ExecutorService;

import static org.jbehave.core.reporters.Format.*;
import static org.jbehave.web.selenium.WebDriverHtmlOutput.WEB_DRIVER_HTML;

public class WebDriverEmbedder extends Embedder {

    private final WebDriverProvider driverProvider = new ManagedWebDriverProvider();
    private final TodoMvc todoMvc = new TodoMvc(driverProvider);
    private final WebDriverSteps lifecycleSteps = new PerStoriesWebDriverSteps(driverProvider);

    @Override
    public EmbedderControls embedderControls() {
        return new EmbedderControls()
                .doVerboseFailures(true);
    }

    @Override
    public ExecutorService executorService() {
        // required by org.jbehave.web.selenium.PerStoriesWebDriverSteps
        return new DirectExecutorService().create(embedderControls());
    }

    @Override
    public Configuration configuration() {
        return new SeleniumConfiguration()
                .useWebDriverProvider(driverProvider)
                .useStoryControls(new StoryControls()
                        .doDryRun(false)
                        .doSkipScenariosAfterFailure(true)
                        .doResetStateBeforeScenario(true)
                        .doResetStateBeforeStory(true))
                .useStoryLoader(new LoadFromClasspath())
                .useParameterConverters(new ParameterConverters()
                        .addConverters(new ParameterConverters.DateConverter(new SimpleDateFormat("yyyy-MM-dd"))))
                .useStoryReporterBuilder(new StoryReporterBuilder()
                        .withFormats(CONSOLE, TXT, STATS, WEB_DRIVER_HTML)
                        .withPathResolver(new FilePrintStreamFactory.ResolveToPackagedName())
                        .withFailureTrace(true)
                        .withFailureTraceCompression(false)
                        .withCrossReference(new CrossReference()));
    }

    @Override
    public InjectableStepsFactory stepsFactory() {
        return new InstanceStepsFactory(
                configuration(),
                new TodoMvcSteps(todoMvc),
                new StoriesLifecycleSteps(driverProvider),
                lifecycleSteps);
    }
}
