package pl.codeleak.samples.jbehave.selenium.todomvc;

import org.jbehave.core.embedder.Embedder;
import org.jbehave.core.junit.JUnitStory;
import pl.codeleak.samples.jbehave.selenium.todomvc.support.WebDriverEmbedder;

public abstract class TodoMvcStory extends JUnitStory {
    @Override
    public Embedder configuredEmbedder() {
        return new WebDriverEmbedder();
    }
}
