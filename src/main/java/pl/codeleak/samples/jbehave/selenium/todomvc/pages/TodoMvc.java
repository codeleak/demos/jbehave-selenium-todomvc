package pl.codeleak.samples.jbehave.selenium.todomvc.pages;

import org.jbehave.web.selenium.WebDriverProvider;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.stream.Collectors;

public class TodoMvc extends AbstractPage {

    public TodoMvc(WebDriverProvider driverProvider) {
        super(driverProvider);
    }

    public void navigateTo() {
        get("http://todomvc.com/examples/vanillajs");
    }

    public void createTodo(String todoName) {
        type(By.className("new-todo"), todoName + Keys.ENTER);
    }

    public void createTodos(String... todoNames) {
        for (String todoName : todoNames) {
            createTodo(todoName);
        }
    }

    public int getTodosLeft() {
        return Integer.parseInt(findElement(By.cssSelector(".todo-count > strong")).getText());
    }

    public boolean todoExists(String todoName) {
        return getTodos().stream().anyMatch(todoName::equals);
    }

    public int getTodoCount() {
        return getTodoElements().size();
    }

    public List<String> getTodos() {
        return getTodoElements()
                .stream()
                .map(WebElement::getText)
                .collect(Collectors.toList());
    }

    public void renameTodo(String todoName, String newTodoName) {
        WebElement todoToEdit = getTodoElementByName(todoName);
        doubleClick(todoToEdit);

        WebElement todoEditInput = find(By.cssSelector("input.edit"), todoToEdit);
        executeScript("arguments[0].value = ''", todoEditInput);

        type(todoEditInput, newTodoName + Keys.ENTER);
    }

    public void removeTodo(String todoName) {
        WebElement todoToRemove = getTodoElementByName(todoName);
        moveToElement(todoToRemove);
        click(By.cssSelector("button.destroy"), todoToRemove);
    }

    public void completeTodo(String todoName) {
        WebElement todoToComplete = getTodoElementByName(todoName);
        click(By.cssSelector("input.toggle"), todoToComplete);
    }

    public void completeAllTodos() {
        click(By.className("toggle-all"));
    }

    public void showActive() {
        click(By.cssSelector("a[href='#/active']"));
    }

    public void showCompleted() {
        click(By.cssSelector("a[href='#/completed']"));
    }

    public void clearCompleted() {
        click(By.className("clear-completed"));
    }

    private List<WebElement> getTodoElements() {
        return findElements(By.cssSelector(".todo-list li"));
    }

    private WebElement getTodoElementByName(String todoName) {
        return getTodoElements()
                .stream()
                .filter(el -> todoName.equals(el.getText()))
                .findFirst()
                .orElseThrow(() -> new AssertionError("Test data missing!"));
    }
}
