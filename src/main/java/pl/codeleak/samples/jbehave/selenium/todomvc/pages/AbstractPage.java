package pl.codeleak.samples.jbehave.selenium.todomvc.pages;

import org.jbehave.web.selenium.WebDriverPage;
import org.jbehave.web.selenium.WebDriverProvider;
import org.openqa.selenium.By;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public abstract class AbstractPage extends WebDriverPage  {

    public AbstractPage(WebDriverProvider driverProvider) {
        super(driverProvider);
    }

    protected WebElement find(By by, SearchContext searchContext) {
        return searchContext.findElement(by);
    }

    protected void click(By by, SearchContext searchContext) {
        WebElement element = searchContext.findElement(by);
        element.click();
    }

    protected void click(By by) {
        findElement(by).click();
    }

    protected void type(By by, CharSequence charSequence) {
        type(findElement(by), charSequence);
    }

    protected void type(WebElement element, CharSequence value) {
        element.sendKeys(value);
    }

    protected void moveToElement(WebElement element) {
        new Actions(getDriverProvider().get()).moveToElement(element).perform();
    }

    protected void doubleClick(WebElement element) {
        new Actions(getDriverProvider().get()).doubleClick(element).perform();
    }
}
