package pl.codeleak.samples.jbehave.selenium.todomvc;

import org.jbehave.core.embedder.Embedder;
import org.jbehave.core.io.StoryFinder;
import org.jbehave.core.junit.JUnitStories;
import pl.codeleak.samples.jbehave.selenium.todomvc.support.WebDriverEmbedder;

import java.util.List;

import static java.util.Arrays.asList;
import static org.jbehave.core.io.CodeLocations.codeLocationFromClass;

public class TodoMvcStories extends JUnitStories {

    @Override
    public Embedder configuredEmbedder() {
        return new WebDriverEmbedder();
    }

    @Override
    protected List<String> storyPaths() {
        return new StoryFinder()
                .findPaths(codeLocationFromClass(this.getClass()).getFile(), asList("**/*.story"), null);
    }
}
