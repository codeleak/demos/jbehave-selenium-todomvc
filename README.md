JBehave and Selenium
====================

Example project for testing TodoMVC with JBehave and Selenium WebDriver

# Prerequisites

- Git
- JDK 11+
- IntelliJ with JBehave Plugin
- Terminal
- Firefox and Chrome browsers

# Run stories in terminal

- Open Terminal
- Run `git clone <repository url>`
- Navigate to `jbehave-selenium-todomvc`
- Run `./mvnw clean test`

> Note: The stories will be executed in `Firefox` browser by default. To change the browser to `Chrome` run: `./mvnw clean test -Dbrowser=chrome`

```
[INFO] Reports view generated with 3 stories (of which 0 pending) containing 2 scenarios (of which 0 pending)
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  9.729 s
[INFO] Finished at: 2020-07-10T13:37:15+02:00
[INFO] ------------------------------------------------------------------------
```

# Running individual stories in IntelliJ

- Open IntelliJ
- Import Maven project
- Run one of Java files located in `pl.codeleak.samples.stories` package

# Adding a new story

- Create `my-story.story` file in `src/main/resources/pl/codeleak/samples/jbehave/selenium/todomvc/stories`
- Optionally create a `Java` class of the `MyStory.java` extending from `pl.codeleak.samples.jbehave.selenium.todomvc.TodoMvcStory` in `src/main/java/pl/codeleak/samples/jbehave/selenium/todomvc/stories`
- Run an individual story via newly created `Java` class or run all stories using `pl.codeleak.samples.jbehave.selenium.todomvc.TodoMvcStories` 